package com.moon.dozer.sonar.test;

import com.moon.dozer.entity.ApplyInfoStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangqing
 * @title: StringTest
 * @projectName moon-demo
 * @description: TODO
 * @date 2021/7/30
 */
public class StringTest {


    /**
     * 原始的 结果
     *
     * @param
     * @return
     */
    public List<String> replaceStringList(List<String> originalList) {
        List<String> resultList = new ArrayList<>(originalList.size());
        for (String s : originalList) {
            resultList.add(s.replace("-", "*"));
        }
        return resultList;
    }

    public List<String> replaceAllStringList(List<String> originalList) {
        List<String> resultList = new ArrayList<>(originalList.size());
        for (String s : originalList) {
            resultList.add(s.replaceAll("-", "*"));
        }
        return resultList;
    }

    public static ApplyInfoStatus codeOf(String code) {

        ApplyInfoStatus[] values = ApplyInfoStatus.values();
        for (ApplyInfoStatus value : values) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }

    public void testError() {
         String init = "Bob is a Bird... Bob is a Plane... Bob is Superman!";
        String changed = init.replaceAll("\\w*\\sis", "It's");
        changed = changed.replaceAll("\\.{3}", ";");
    }


}
