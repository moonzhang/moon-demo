package com.moon.dozer.sonar.test;

import com.moon.dozer.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author zhangqing
 * @title: GenericTest
 * @projectName moon-demo
 * @description: 泛型检查
 * @date 2021/7/30
 */
public class GenericTest {

    public  String getUserName(List userList){
        return  ((UserEntity)getUserList().get(0)).getName();
    }


    public List getUserList(){
        List userList = new ArrayList();
        userList.add(new UserEntity());
        userList.add(new UserEntity());
        userList.add(new UserEntity());

        return userList;
    }
}
