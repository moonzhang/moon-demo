package com.moon.dozer.sonar.test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author zhangqing
 * @title: TestMain
 * @projectName moon-demo
 * @description: 测试启动
 * @date 2021/7/30
 */
public class TestMain {
    private static List<String> getDataUUid(){
        List<String> strings = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            strings.add(UUID.randomUUID().toString());
        }
        return strings;
    }
    public static void main(String[] args) {
        
        
        
        StringTest stringTest = new StringTest();
        long starTime = System.currentTimeMillis();
        stringTest.replaceAllStringList(getDataUUid());
        System.out.println(System.currentTimeMillis()-starTime);
        starTime = System.currentTimeMillis();
        stringTest.replaceStringList(getDataUUid());
        System.out.println(System.currentTimeMillis()-starTime);
    }
}
