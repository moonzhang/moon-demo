package com.moon.dozer.sonar.test.util;

import java.util.HashMap;
import java.util.Map;

public class TenantUtil {



    private static Map<String, String> tenantIdMap = new HashMap<>();

    static {

        tenantIdMap.put("440100000000", "10190127");
        tenantIdMap.put("440200000000", "10190113");
        tenantIdMap.put("440300000000", "10190128");
        tenantIdMap.put("440400000000", "10190126");
        tenantIdMap.put("440500000000", "10190109");
        tenantIdMap.put("440600000000", "10190129");
        tenantIdMap.put("440700000000", "10190112");
        tenantIdMap.put("440800000000", "10190116");
        tenantIdMap.put("440900000000", "10190103");
        tenantIdMap.put("441200000000", "10190108");
        tenantIdMap.put("441300000000", "10190115");
        tenantIdMap.put("441400000000", "10190107");
        tenantIdMap.put("441500000000", "10190110");
        tenantIdMap.put("441600000000", "10190114");
        tenantIdMap.put("441700000000", "10190105");
        tenantIdMap.put("441800000000", "10190104");
        tenantIdMap.put("441900000000", "10190130");

        tenantIdMap.put("442000000000", "10190125");
        tenantIdMap.put("445100000000", "10190111");
        tenantIdMap.put("445200000000", "10190102");
        tenantIdMap.put("445300000000", "10190106");
        tenantIdMap.put("845000000000", "10190124");
        tenantIdMap.put("LS440000002000", "10190123");

    }

    public static String getTitle(String tenantId) {
        return tenantIdMap.get(tenantId);
    }
}
